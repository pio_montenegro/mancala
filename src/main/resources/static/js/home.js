'use strict';

var app = angular.module('mancala', [])
app.controller('gameController', function($scope, $http) {

    // this function initialise a new Game
    $scope.startGame = function(){
        $scope.errorMessage = "";
        $http.post("/mancala/start").then(function(resp){
            console.log(resp.data);
            $scope.gameBoard = resp.data.gameBoard;
        }).catch(function(resp){
            console.log(resp);
        })
    }

    // this function plays a move on the board
    $scope.play = function(playerId, selectedPit){

        $scope.errorMessage = "";
        $http.put("/mancala/play/"+playerId+"/"+selectedPit+"").then(function(resp){
            console.log(resp.data);
            $scope.game = resp.data;
            $scope.pits = resp.data.gameBoard.pits;

            $scope.total = 0
            angular.forEach(resp.data.gameBoard.pits, function(pit, key) {
                $scope.total += pit.currentNumberOfStones;
            });

        }).catch(function(resp){
            console.log(resp);
        })
    }
});