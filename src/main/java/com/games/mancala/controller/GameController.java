package com.games.mancala.controller;

import com.games.mancala.facade.GameFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * This class is the controller which exposes the rest endpoints to implement the Mancala game
 * Created by pio.montenegro on 23/11/2018.
 */
@RestController
@RequestMapping("/mancala")
public class GameController {

    private static final Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameFacade gameFacade;

    /**
     * This endpoint exposes a method to start a new game
     * @param numberOfStones configurable initial number of stones per pit
     * @return ResponseEntity object
     */
    @PostMapping(value = "/start")
    public ResponseEntity start(@RequestParam(name = "numberOfStones", defaultValue = "6", required = false) Integer numberOfStones){
        logger.info("Starting new Mancala game at {}", new Date());
        return ResponseEntity.ok().body(gameFacade.initGame(numberOfStones));
    }

    /**
     * This endpoint exposes a method to move the stones to a target pit, the player making the move is identified by
     * the playerId
     * @param playerId the id of the player making the move
     * @param selectedPit the target pit of the current move
     * @return ResponseEntity object
     */
    @PutMapping("/play/{playerId}/{selectedPit}")
    public ResponseEntity play(@PathVariable String playerId, @PathVariable Integer selectedPit){
        return ResponseEntity.ok().body(gameFacade.moveToPit(playerId, selectedPit));
    }
}
