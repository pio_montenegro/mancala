package com.games.mancala.service.rule;

import com.games.mancala.model.Board;
import com.games.mancala.model.Player;
import org.springframework.stereotype.Service;

/**
 * This class implements the RuleEngine interface and it defines the applicable rules on a Board object
 * Created by pio.montenegro on 23/11/2018.
 */
@Service
public class RuleEngineImpl implements RuleEngine {

    private final String playerOneId = "PLAYER_ONE";
    private final String playerTwoId = "PLAYER_TWO";
    private static final Integer PLAYER_ONE_START = 1;
    private static final Integer PLAYER_TWO_START = 8;
    private static final Integer PLAYER_ONE_END = 6;
    private static final Integer PLAYER_TWO_END = 13;

    /**
     * This method returns true if the game is over, false otherwise
     * @param board the current board
     * @param currentPlayer the current player
     * @return boolean
     */
    public boolean isGameOver(Board board, Player currentPlayer) {
        return false;
    }

    /**
     * This method returns true if a move is legal, false otherwise
     * @param board the board of the current Game
     * @param currentPlayer the current Player attempting a move
     * @param targetPit the index of the target Pit
     * @return boolean
     */
    public boolean isLegalMove(Board board, Player currentPlayer, int targetPit) {

        if (currentPlayer != null) {

            String currentPlayerId = currentPlayer.getId();

            // current player is Player one
            if (playerOneId.equals(currentPlayerId) && PLAYER_ONE_START <= targetPit && targetPit <= PLAYER_ONE_END){
                return  true;
            }

            //current player is Player two
            else if (playerTwoId.equals(currentPlayerId) && PLAYER_TWO_START <= targetPit && targetPit <= PLAYER_TWO_END) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method returns true if an extra move is allowed, false otherwise
     * @param board the board of the current Game
     * @param currentPlayer the current Player attempting a move
     * @param targetPit the index of the target Pit
     * @return boolean
     */
    public boolean isExtraMoveAllowed(Board board, Player currentPlayer, int targetPit) {
        return false;
    }

    /**
     * This method returns true if the currentPlayer is allowed to capture stones from the opponent pit
     * @param board the board of the current Game
     * @param currentPlayer the current Player attempting a move
     * @param targetPit the index of the target Pit
     * @return boolean
     */
    @Override
    public boolean isOpponentStonesCaptureAllowed(Board board, Player currentPlayer, int targetPit) {
        return false;
    }
}
