package com.games.mancala.service.rule;

import com.games.mancala.model.Board;
import com.games.mancala.model.Player;

/**
 * Created by pio.montenegro on 23/11/2018.
 */
public interface RuleEngine {

    boolean isGameOver(Board board, Player currentPlayer);

    boolean isLegalMove(Board board, Player currentPlayer, int targetPit);

    boolean isExtraMoveAllowed(Board board, Player currentPlayer, int targetPit);

    boolean isOpponentStonesCaptureAllowed(Board board, Player currentPlayer, int targetPit);
}
