package com.games.mancala.service.move;

import com.games.mancala.model.Board;
import com.games.mancala.model.Game;
import com.games.mancala.model.Player;
import org.springframework.stereotype.Service;

/**
 * This class is responsible for the implementation of all the methods allowing a Player to interact with a Board object
 * Created by pio.montenegro on 23/11/2018.
 */
@Service
public class GameEngineImpl implements GameEngine{

    /**
     * This method moves stones on the board to a target pit, it returns true if the move is executed correctly
     * false otherwise
     * @param board the board of the current Game
     * @param currentPlayer the Player moving stones
     * @param targetPit the index of the Pit where stones are moved to
     * @return
     */
    public boolean moveStones(Board board, Player currentPlayer, int targetPit) {
        if (board != null) {
            // logic to move the stones of the current playes on the board to a targetPit
            //TODO: refactor this method to throw Exceptions
        }
        return false;
    }

    /**
     * This method returns a Player in a Board given its id
     * @param board the board of the current Game
     * @param playerId the id of the Player to look for
     * @return Player
     */
    public Player getPlayerById(Board board, String playerId) {
        //TODO: finalise implementation
        return null;
    }

    /**
     * Thie method creates a new instance of a Game object
     * @param playerOneId the id of the first player
     * @param playerTwoId the id of the second player
     * @param stonesPerPit the number of stones per pit
     * @return
     */
    public Game createGame(String playerOneId, String playerTwoId, Integer stonesPerPit) {
        Player playerOne = new Player(playerOneId);
        Player playerTwo = new Player(playerTwoId);
        Board currentBoard = new Board(playerOne, playerTwo, stonesPerPit);
        Game currentGame = new Game(currentBoard);
        return currentGame;
    }

}
