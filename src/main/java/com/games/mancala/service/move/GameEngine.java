package com.games.mancala.service.move;

import com.games.mancala.model.Board;
import com.games.mancala.model.Game;
import com.games.mancala.model.Player;

/**
 * Created by pio.montenegro on 23/11/2018.
 */
public interface GameEngine {

    boolean moveStones(Board board, Player currentPlayer, int targetPit);

    Player getPlayerById(Board board, String playerId);

    Game createGame(String playerOneId, String playerTwoId, Integer stonesPerPit);
}
