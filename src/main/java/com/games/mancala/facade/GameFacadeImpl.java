package com.games.mancala.facade;

import com.games.mancala.model.Board;
import com.games.mancala.model.Game;
import com.games.mancala.model.Player;
import com.games.mancala.service.move.GameEngine;
import com.games.mancala.service.rule.RuleEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class implements the methods defined in the GameFacadeInterface
 * Created by pio.montenegro on 23/11/2018.
 */
@Service
public class GameFacadeImpl implements GameFacade {

    private final static String playerOneId = "PLAYER_ONE";
    private final static String playerTwoId = "PLAYER_TWO";

    private static Game currentGame;

    private static final Logger logger = LoggerFactory.getLogger(GameFacadeImpl.class);

    @Autowired
    GameEngine gameEngine;

    @Autowired
    RuleEngine ruleEngine;

    /**
     * This method initialises and returns a new Game object with a defined number of stonesPerPit
     * @param stonesPerPit number of initial stones per pit
     * @return Game
     */
    public Game initGame(Integer stonesPerPit){

        logger.info("Called initGame with {} stones per pit", stonesPerPit);
        currentGame = gameEngine.createGame(playerOneId, playerTwoId, stonesPerPit);

        return currentGame;
    }

    /**
     * This method moves the stones of a player whose id is playerId to a pit whose id is targetPit
     * @param playerId the id of the Player making the move
     * @param targetPit the index ot the target Pit
     * @return Game
     */
    public Game moveToPit(String playerId, Integer targetPit) {
        Board gameBoard = currentGame.getGameBoard();

        if (gameBoard != null) {

            Player currentPlayer = gameEngine.getPlayerById(gameBoard, playerId);

            if (ruleEngine.isLegalMove(gameBoard, currentPlayer, targetPit)) {
                gameEngine.moveStones(gameBoard, currentPlayer, targetPit);

                if (ruleEngine.isGameOver(gameBoard, currentPlayer)){
                    // handle the game winner
                }
            }
        }
        return currentGame;
    }

    public Game getCurrentGame() {
        return currentGame;
    }
}
