package com.games.mancala.facade;

import com.games.mancala.model.Game;

/**
 * This interface defines the methods to play a game
 * Created by pio.montenegro on 23/11/2018.
 */
public interface GameFacade {

    Game initGame(Integer stonesPerPit);

    Game moveToPit(String playerId, Integer targetPit);



}
