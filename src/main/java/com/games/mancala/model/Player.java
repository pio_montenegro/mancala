package com.games.mancala.model;

import java.util.Date;

/**
 * This class represents a Player with an id, a name and the date of the last login
 * Created by pio.montenegro on 23/11/2018.
 */
public class Player {

    private String id;
    private String name;
    private Date lastLogin;
    private final Integer PLAYER_ONE_INDEX = 1;
    private final Integer PLAYER_TWO_INDEX = 2;


    public Player(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPlayerOneIndex() {
        return PLAYER_ONE_INDEX;
    }

    public Integer getPlayerTwoIndex() {
        return PLAYER_TWO_INDEX;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
}
