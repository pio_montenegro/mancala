package com.games.mancala.model;

/**
 * This class represents a Pit with a defined number of stones
 * Created by pio.montenegro on 23/11/2018.
 */
public class Pit {

    private Integer index;
    private Integer currentNumberOfStones;

    public Pit(Integer index, Integer currentNumberOfStones) {
        this.index = index;
        this.currentNumberOfStones = currentNumberOfStones;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getCurrentNumberOfStones() {
        return currentNumberOfStones;
    }

    public void setCurrentNumberOfStones(Integer currentNumberOfStones) {
        this.currentNumberOfStones = currentNumberOfStones;
    }
}
