package com.games.mancala.model;

/**
 * This class defines a game, it holds also the current game board
 * Created by pio.montenegro on 23/11/2018.
 */
public class Game {

    private Board gameBoard;

    public Game(Board gameBoard) {
        this.gameBoard = gameBoard;
    }

    public Board getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(Board gameBoard) {
        this.gameBoard = gameBoard;
    }
}
