package com.games.mancala.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

/**
 * This class defines an instance of a board game
 * Created by pio.montenegro on 23/11/2018.
 */
public class Board {

    private static final Integer PLAYER_ONE_BASE = 7;
    private static final Integer PLAYER_TWO_BASE = 14;
    private static final Integer PLAYER_ONE_START = 1;
    private static final Integer PLAYER_TWO_START = 8;
    private static final Integer INIT_STONES_PER_PIT = 6;
    private static final Integer INIT_STONES_PER_BASE = 0;


    private List<Player> players;
    private Map<Integer, Pit> pits;

    public Board(Player playerOne, Player playerTwo, Integer stonesPerPit) {
        this.players = Arrays.asList (playerOne, playerTwo);
        this.pits = initPits(stonesPerPit);
    }

    private Map<Integer, Pit> initPits(Integer stonesPerPit) {
        this.pits = new ConcurrentHashMap<>();

        IntStream.range(PLAYER_ONE_START,PLAYER_ONE_BASE).forEach(i -> pits.put(i,new Pit(i, stonesPerPit)));
        IntStream.range(PLAYER_TWO_START,PLAYER_TWO_BASE).forEach(j -> pits.put(j, new Pit(j, stonesPerPit)));
        pits.put(PLAYER_ONE_BASE, new Pit(PLAYER_ONE_BASE, INIT_STONES_PER_BASE));
        pits.put(PLAYER_TWO_BASE, new Pit(PLAYER_TWO_BASE, INIT_STONES_PER_BASE));

        return pits;

    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Map<Integer, Pit> getPits() {
        return pits;
    }

    public void setPits(Map<Integer, Pit> pits) {
        this.pits = pits;
    }
}
