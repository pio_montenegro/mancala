package com.games.mancala.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Created by pio.montenegro on 23/11/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private final Integer STONES_PER_PIT = 6;
    private final Integer STONES_PER_BASE = 0;

    @Test
    public void initGameTest() throws Exception {
        MockHttpServletRequestBuilder startRequest = MockMvcRequestBuilders.post("/mancala/start");

        mockMvc.perform(startRequest)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.gameBoard.pits.1.currentNumberOfStones").value(STONES_PER_PIT))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gameBoard.pits.7.currentNumberOfStones").value(STONES_PER_BASE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gameBoard.pits.14.currentNumberOfStones").value(STONES_PER_BASE))
                .andReturn();
    }
}

