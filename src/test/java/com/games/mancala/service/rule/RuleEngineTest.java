package com.games.mancala.service.rule;

import com.games.mancala.model.Board;
import com.games.mancala.model.Player;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by pio.montenegro on 23/11/2018.
 */
public class RuleEngineTest {

    private Board currentBoard;
    private Player currentPlayerOne;
    private Player currentPlayerTwo;
    private final String playerOneId = "PLAYER_ONE";
    private final String playerTwoId = "PLAYER_TWO";
    private final Integer currentNumOfStones = 6;
    private RuleEngine ruleEngineToTest = new RuleEngineImpl();

    private void init() {
        currentBoard = new Board(currentPlayerOne, currentPlayerTwo, currentNumOfStones);
        currentPlayerOne = new Player(playerOneId);
        currentPlayerTwo = new Player(playerTwoId);
    }

    @Test
    public void isLegalMoveOK() {

        //given
        this.init();

        //when

        boolean isLegalMove = ruleEngineToTest.isLegalMove(currentBoard, currentPlayerOne, 5);

        //then
        Assert.assertEquals(true, isLegalMove);
    }

    @Test
    public void isLegalMoveNOK() {

        //given
        this.init();

        //when

        boolean isLegalMove = ruleEngineToTest.isLegalMove(currentBoard, currentPlayerOne, 9);

        //then
        Assert.assertEquals(false, isLegalMove);
    }

}
